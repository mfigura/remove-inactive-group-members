# Remove inactive members from GitLab.com group

Recursively remove all inactive users from a GitLab.com group, its subgroups and projects using the GitLab API. Can be run as scheduled pipeline to continuously remove inactive users.

The script uses the [user events API](https://docs.gitlab.com/ee/api/events.html#get-user-contribution-events) to determine activity.

That means that users with **private profiles won't be able to be blocked**, as their user events are invisible to the script.

**Be aware that read-only users or rarely used bots that don't show activity will be removed by this script**

## Configuration

- Export / fork repository.
- Add a GitLab **group owner** API token to CI/CD variables named `GITLAB_TOKEN`. Make sure it is "masked".
This token will be used to query the API for group and project members.
The token **must** be an group owner token to be able to remove members of the group them.
- Add your GitLab instance URL to CI/CD variables named `GITLAB`.
- Configure `INACTIVITY_DAYS` to the number of days of inactivity that are acceptable before removing a user
- notice the Job is tagged `local` for my testing purposes. Make sure your runners pick it up.
- Schedule the pipeline to run it weekly or your desired interval, using Pipelines -> Schedules
- Upvote this issue: https://gitlab.com/gitlab-org/gitlab/-/issues/211754

## Usage

`python3 remove-inactive-group-members.py $GITLAB $GITLAB_TOKEN $GROUP $INACTIVITY_DAYS --dryrun`

I recommend running this manually and doing a dryrun first

## Parameters

- `$GITLAB`: URL of the GitLab instance
- `$GITLAB_TOKEN`: Group owner API token
- `$GROUP`: ID or namespace of the group this script should run on
- `$INACTIVITY_DAYS`: Number of days an account can be inactive before it will be removed from the group and projects
- `--dryrun`: Dryrun mode. Only compile the CSV report of users to remove, don't actually remove anyone.

## What does it do?

* Request all subgroups and projects of a group
* Get all members of the group, its subgroups and projects
  * don't get the top group owners and the owner of the API token to not lock yourself out of the group
* For all members:
  * Query the [user events API](https://docs.gitlab.com/ee/api/events.html#get-user-contribution-events) and get the last contribution made.
  * if the days since that contribution are more than specified in $INACTIVITY_DAYS, i.e. the user has not contributed for more days than specified, remove the user from all groups and projects they are a direct member of
* For all members to be removed:
  * Add them to a CSV report

## Disclaimer

This script is provided for educational purposes. It is **not supported by GitLab**. However, you can create an issue if you need help or propose a Merge Request. This script removes members of your group and thus their access to projects, which can be disruptive if used inappropriately (for example using a very small `INACTIVITY_DAYS` interval). **Always run it in dryrun mode first, to understand the impact to your users.

This script produces data file artifacts containing user names. Please use this script and its outputs with caution and in accordance to your local data privacy regulations.
